Nguyen Khoi Tran(260350260)
ECSE427-Assignment4(Using pipes)

In order to run this program, two command line arguments must be given;
the input file and an output file name.

Terminal output:
khoi.tran@linux:~/ecse427/a4$ gcc pipe.c -o pipe
khoi.tran@linux:~/ecse427/a4$ ./pipe input.txt output.txt
Read from file: THIS IS A TEST.
Filter Read: THIS IS A TEST.
Converted to: this is a test.
Sink read: this is a test.
Read from file: I HAVE ALLCAPS HERE
Read from file: SHOULD BE LOWERCASE IN OUTPUT.TXT.
Read from file: MORE LINES.
Filter Read: I HAVE ALLCAPS HERE
Converted to: i have allcaps here
Sink read: i have allcaps here
Filter Read: SHOULD BE LOWERCASE IN OUTPUT.TXT.
Converted to: should be lowercase in output.txt.
Sink read: should be lowercase in output.txt.
Read from file: I THINK I NEED MORE LINES TO TEST.
Filter Read: MORE LINES.
Converted to: more lines.
Sink read: more lines.
Read from file: COPY THIS INTO OUTPUT.TXT.
Read from file: -Khoi Tran
Finished Reading from file.
Filter Read: I THINK I NEED MORE LINES TO TEST.
Converted to: i think i need more lines to test.
Filter Read: COPY THIS INTO OUTPUT.TXT.
Converted to: copy this into output.txt.
Filter Read: -Khoi Tran
Converted to: -khoi tran
Sink read: i think i need more lines to test.
FINISHED FILTER.
Sink read: copy this into output.txt.
Sink read: -khoi tran
FINISHED SINK.
Finished everything!

This outputs a filter output.txt that correctly converts input.txt
changing all uppercase letters to lower case letters.
sleep(rand()%3) was used to build up information in the pipes.
