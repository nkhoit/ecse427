#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h> 
#include <signal.h>
#include "elapsed.h"
#define FAIL 0  

void initial();
long elapsed_usecs(struct itimerval it);
void sighandle(int sig);
void sigcount(int sig);
long unsigned int fibonacci(unsigned int n);

Proctimer timer; 

int main(int argc, char *argv[]){
  struct itimerval old;
  struct itimerval set;
  long elap_real, elap_virt, elap_prof;
  int pid1, pid2;
  int status;
  long unsigned int fib;
  unsigned int fibarg;

  /* check for minimal # of args */
  if(argc < 2){
    printf("Usage: %s fib#\n", argv[0]);
    exit(FAIL);
  }
  fibarg = atoi(argv[1]);

  set.it_interval.tv_sec = 1;
  set.it_interval.tv_usec = 0;
  set.it_value.tv_sec = 1;
  set.it_value.tv_usec = 0;
  
  printf("set = intsec=%ld, intusec=%ld, valsec=%ld, valusec=%ld\n", 
	 set.it_interval.tv_sec, set.it_interval.tv_usec, 
	 set.it_value.tv_sec, set.it_value.tv_usec);
  //initialize signal counters
  initial(timer);
  //enable signal handlers parent
  signal(SIGALRM, sighandle);
  signal(SIGVTALRM, sighandle);
  signal(SIGPROF, sighandle);
  //set timers parent
  setitimer(ITIMER_REAL, &set, &old);
  setitimer(ITIMER_VIRTUAL, &set, &old);
  setitimer(ITIMER_PROF, &set, &old);

  //spawn first child
  pid1= fork();
  if(pid1 == 0){

    //spawn second child
    pid2= fork();
    if(pid2 == 0){
      //enable signal handlers child2
      signal(SIGALRM, sighandle);
      signal(SIGVTALRM, sighandle);
      signal(SIGPROF, sighandle);
      //set timers child2
      setitimer(ITIMER_REAL, &set, &old);
      setitimer(ITIMER_VIRTUAL, &set, &old);
      setitimer(ITIMER_PROF, &set, &old);
      //start child2 fib
      fib = fibonacci(fibarg);
    
      getitimer(ITIMER_REAL, &(timer.real_it));
      getitimer(ITIMER_VIRTUAL, &(timer.virt_it));
      getitimer(ITIMER_PROF, &(timer.prof_it));

      elap_real = elapsed_usecs(timer.real_it); 
      if(elap_real!=-1){
	elap_real = elap_real + (timer.real_secs * 1000000);
	printf("elapsed_real=%ld\n", elap_real);
      }
      elap_virt = elapsed_usecs(timer.virt_it); 
      if(elap_virt!=-1){
	elap_virt = elap_virt + (timer.virt_secs * 1000000);
	printf("elapsed_virt=%ld\n", elap_virt);
      }
      elap_prof = elapsed_usecs(timer.prof_it); 
      if(elap_prof!=-1){
	elap_prof = elap_prof + (timer.prof_secs * 1000000);
	printf("elapsed_prof=%ld\n", elap_prof);
      }
      
      printf("child2 fib=%ld, real time=%ld, cpu time=%ld, kernel time=%ld, user time=%ld\n", fib, elap_real, elap_prof, (elap_prof-elap_virt), 
	     elap_virt);
      exit(0);
    }else{
      //enable signal handlers child1
      signal(SIGALRM, sighandle);
      signal(SIGVTALRM, sighandle);
      signal(SIGPROF, sighandle);
      //set timers child1
      setitimer(ITIMER_REAL, &set, &old);
      setitimer(ITIMER_VIRTUAL, &set, &old);
      setitimer(ITIMER_PROF, &set, &old);
      //start child1 fib
      fib = fibonacci(fibarg);
      
      getitimer(ITIMER_REAL, &(timer.real_it));
      getitimer(ITIMER_VIRTUAL, &(timer.virt_it));
      getitimer(ITIMER_PROF, &(timer.prof_it));

      waitpid(pid2, &status, 0);
      
      elap_real = elapsed_usecs(timer.real_it); 
      if(elap_real!=-1){
	elap_real = elap_real + (timer.real_secs * 1000000);
	printf("elapsed_real=%ld\n", elap_real);
      }
      elap_virt = elapsed_usecs(timer.virt_it); 
      if(elap_virt!=-1){
	elap_virt = elap_virt + (timer.virt_secs * 1000000);
	printf("elapsed_virt=%ld\n", elap_virt);
      }
      elap_prof = elapsed_usecs(timer.prof_it); 
      if(elap_prof!=-1){
	elap_prof = elap_prof + (timer.prof_secs * 1000000);
	printf("elapsed_prof=%ld\n", elap_prof);
      }
      
      printf("child1 fib=%ld, real time=%ld, cpu time=%ld, kernel time=%ld, user time=%ld\n", fib, elap_real, elap_prof, (elap_prof-elap_virt), 
	     elap_virt);
      exit(0);
    }
  }else{

    //start parent fib
    fib = fibonacci(fibarg);
    //wait for children
    waitpid(pid1, &status, 0);
    
    getitimer(ITIMER_REAL, &(timer.real_it));
    getitimer(ITIMER_VIRTUAL, &(timer.virt_it));
    getitimer(ITIMER_PROF, &(timer.prof_it));

    elap_real = elapsed_usecs(timer.real_it); 
    if(elap_real!=-1){
      elap_real = elap_real + (timer.real_secs * 1000000);
      printf("elapsed_real=%ld\n", elap_real);
    }
    elap_virt = elapsed_usecs(timer.virt_it); 
    if(elap_virt!=-1){
      elap_virt = elap_virt + (timer.virt_secs * 1000000);
      printf("elapsed_virt=%ld\n", elap_virt);
    }
    elap_prof = elapsed_usecs(timer.prof_it); 
    if(elap_prof!=-1){
      elap_prof = elap_prof + (timer.prof_secs * 1000000);
      printf("elapsed_prof=%ld\n", elap_prof);
    }

    printf("parent fib=%ld, real time=%ld, cpu time=%ld, kernel time=%ld, user time=%ld\n", fib, elap_real, elap_prof, (elap_prof-elap_virt), 
	   elap_virt); 

    return(0);
  }
}

void initial(){
  timer.real_secs = 0;
  timer.virt_secs = 0;
  timer.prof_secs = 0;
}

/* Converts all to usecs and then does the math to find elapsed
 * returns elapsed value or -1 if error occurs
 */
long elapsed_usecs(struct itimerval it){
  long slice, left, elapsed;
  
  slice = ((it.it_interval.tv_sec * 1000000) + it.it_interval.tv_usec);
  left = ((it.it_value.tv_sec * 1000000) + it.it_value.tv_usec);

  if(left> slice){
    printf("error: current > slice\n");
    return(-1);
  }
  elapsed = slice-left;
  
  return(elapsed);
}
void sighandle(int sig){
  sigcount(sig);
}
void sigcount(int sig){
  switch(sig){
  case SIGALRM:
    timer.real_secs++;
    break;
  case SIGVTALRM:
    timer.virt_secs++;
    break;
  case SIGPROF:
    timer.prof_secs++;
    break;
  }
}

long unsigned int fibonacci(unsigned int n){
  if(n==0)
    return 0;
  else if(n==1||n==2)
    return 1;
  else
    return(fibonacci(n-1) + fibonacci(n-2));
}
