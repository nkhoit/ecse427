#include <stdio.h>
#include <sys/time.h>

typedef struct itimerval Itimerval;
typedef struct timeval Timeval;

typedef struct{
  long real_secs;
  long virt_secs;
  long prof_secs;
  Itimerval real_it;
  Itimerval virt_it;
  Itimerval prof_it;
}Proctimer;
