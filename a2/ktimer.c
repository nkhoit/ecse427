/////////////////////////////////////////
//
//
//   ECSE427-Assignment2
//   Nguyen Khoi Tran (260350260)   
//
//
/////////////////////////////////////////



#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <signal.h>

long unsigned int fibonacci(unsigned int n);
long elapsed_usecs(struct itimerval timer);
void p_handler(int signo);
void c1_handler(int signo);
void c2_handler(int signo);

//The following variables are used to record the accumulated times. They are set by the signal handlers and are read by the processes when they report the results.
static long p_realt_secs = 0, c1_realt_secs=0, c2_realt_secs =0;
static long p_virtt_secs = 0, c1_virtt_secs=0, c2_virtt_secs =0;
static long p_proft_secs = 0, c1_proft_secs=0, c2_proft_secs =0;
static struct itimerval p_realt, c1_realt, c2_realt;
static struct itimerval p_virtt, c1_virtt, c2_virtt;
static struct itimerval p_proft, c1_proft, c2_proft;

int main(int argc,char **argv){
  long unsigned fib = 1;
  int pid1, pid2;
  unsigned int fibarg;
  int status;
  
  //get command line argument, fibarg (the value of N in the problem statement
  if(argc<2){
    printf("Usage: %s #\n",argv[0]);
    exit(0);
  }
  fibarg=atoi(argv[1]);
  
  //Initialize parent, child1 and child 2 timer values
  //These are initialized to 999999usec so every 1msec
  //it'll raise its respective signal
  
  //parent
  p_realt.it_interval.tv_sec = 0;  
  p_realt.it_interval.tv_usec = 999999;  
  p_realt.it_value.tv_sec = 0;  
  p_realt.it_value.tv_usec = 999999;  
  p_virtt.it_interval.tv_sec = 0;  
  p_virtt.it_interval.tv_usec = 999999;  
  p_virtt.it_value.tv_sec = 0;  
  p_virtt.it_value.tv_usec = 999999;  
  p_proft.it_interval.tv_sec = 0;  
  p_proft.it_interval.tv_usec = 999999;  
  p_proft.it_value.tv_sec = 0;  
  p_proft.it_value.tv_usec = 999999;  

  //child 1
  c1_realt.it_interval.tv_sec = 0;
  c1_realt.it_interval.tv_usec = 999999;
  c1_realt.it_value.tv_sec = 0;
  c1_realt.it_value.tv_usec = 999999;
  c1_virtt.it_interval.tv_sec = 0;
  c1_virtt.it_interval.tv_usec = 999999;
  c1_virtt.it_value.tv_sec = 0;
  c1_virtt.it_value.tv_usec = 999999;
  c1_proft.it_interval.tv_sec = 0;
  c1_proft.it_interval.tv_usec = 999999;
  c1_proft.it_value.tv_sec = 0;
  c1_proft.it_value.tv_usec = 999999;

  //child 2
  c2_realt.it_interval.tv_sec = 0;
  c2_realt.it_interval.tv_usec = 999999;
  c2_realt.it_value.tv_sec = 0;
  c2_realt.it_value.tv_usec = 999999;
  c2_virtt.it_interval.tv_sec = 0;
  c2_virtt.it_interval.tv_usec = 999999;
  c2_virtt.it_value.tv_sec = 0;
  c2_virtt.it_value.tv_usec = 999999;
  c2_proft.it_interval.tv_sec = 0;
  c2_proft.it_interval.tv_usec = 999999;
  c2_proft.it_value.tv_sec = 0;
  c2_proft.it_value.tv_usec = 999999;
  
  //Enable parent's signal handlers
  signal(SIGALRM,p_handler);
  signal(SIGVTALRM,p_handler);
  signal(SIGPROF,p_handler);

  
  //Set parent's itimers
  if(setitimer(ITIMER_REAL,&p_realt,NULL)== -1) 
    perror("parent real timer set error\n");
  if(setitimer(ITIMER_VIRTUAL,&p_virtt,NULL)== -1)
    perror("parent virtual timer set error\n");
  if(setitimer(ITIMER_PROF,&p_proft,NULL)== -1)
    perror("parent profile timer set error\n");

  pid1=fork();
  
  if(pid1 == 0){
    //Enable child 1 signal handlers (disable parent handlers)
    signal(SIGALRM,c1_handler);
    signal(SIGVTALRM,c1_handler);
    signal(SIGPROF,c1_handler);

    //Set child 1 itimers
    if(setitimer(ITIMER_REAL,&c1_realt,NULL)== -1) 
      perror("child 1 real timer set error\n");
    if(setitimer(ITIMER_VIRTUAL,&c1_virtt,NULL)== -1)
      perror("child 1 virtual timer set error\n");
    if(setitimer(ITIMER_PROF,&c1_proft,NULL)== -1)
      perror("child 1 profile timer set error\n");

    //Start child 1 on the fibonacci program
    fib=fibonacci(fibarg);

    //Read child 1 itimer values and report them
    getitimer(ITIMER_REAL,&c1_realt);
    getitimer(ITIMER_VIRTUAL,&c1_virtt);
    getitimer(ITIMER_PROF,&c1_proft);

    printf("\n");
    printf("Child 1 fib = %ld, real time = %ld sec, %ld msec, %ld usec\n",
      fib,
      c1_realt_secs,
      elapsed_usecs(c1_realt)/1000,
      elapsed_usecs(c1_realt)%1000);

    printf("Child 1 fib = %ld, cpu time = %ld sec, %ld msec, %ld usec\n",
      fib, 
      c1_proft_secs,
      elapsed_usecs(c1_proft)/1000,
      elapsed_usecs(c1_proft)%1000);

    printf("Child 1 fib = %ld, user time = %ld sec, %ld msec, %ld usec\n",
      fib,
      c1_virtt_secs,
      elapsed_usecs(c1_virtt)/1000,
      elapsed_usecs(c1_virtt)%1000);

    printf("Child 1 fib = %ld, kernel time = %ld sec, %ld msec, %ld usec\n",
      fib,
      c1_proft_secs-c1_virtt_secs,
      (elapsed_usecs(c1_proft)-elapsed_usecs(c1_virtt))/1000,
      (elapsed_usecs(c1_proft)-elapsed_usecs(c1_virtt))%1000);

    fflush(stdout);
    exit(0);
  }else{
    pid2=fork();
    if(pid2==0){
      //Enable child 2 signal handlers
      signal(SIGALRM,c2_handler);
      signal(SIGVTALRM,c2_handler);
      signal(SIGPROF,c2_handler);

      //Set child 2 itimers
      if(setitimer(ITIMER_REAL,&c2_realt,NULL)== -1) 
        perror("child 2 real timer set error\n");
      if(setitimer(ITIMER_VIRTUAL,&c2_virtt,NULL)== -1)
        perror("child 2 virtual timer set error\n");
      if(setitimer(ITIMER_PROF,&c2_proft,NULL)== -1)
        perror("child 2 profile timer set error\n");


      //Start child 2 on the fibonacci program
      fib=fibonacci (fibarg);
      // Read child 2 itimer values and report them
      getitimer(ITIMER_REAL,&c2_realt);
      getitimer(ITIMER_VIRTUAL,&c2_virtt);
      getitimer(ITIMER_PROF,&c2_proft);

      printf("\n");
      printf("Child 2 fib = %ld, real time = %ld sec, %ld msec, %ld usec\n",
        fib,
        c2_realt_secs,
        elapsed_usecs(c2_realt)/1000,
        elapsed_usecs(c2_realt)%1000);

      printf("Child 2 fib = %ld, cpu time = %ld sec, %ld msec, %ld usec\n",
        fib, 
        c2_proft_secs,
        elapsed_usecs(c2_proft)/1000,
        elapsed_usecs(c2_proft)%1000);

      printf("Child 2 fib = %ld, user time = %ld sec, %ld msec, %ld usec\n",
        fib,
        c2_virtt_secs,
        elapsed_usecs(c2_virtt)/1000,
        elapsed_usecs(c2_virtt)%1000);

      printf("Child 2 fib = %ld, kernel time = %ld sec, %ld msec, %ld usec\n",
        fib,
        c2_proft_secs-c2_virtt_secs,
        (elapsed_usecs(c2_proft)-elapsed_usecs(c2_virtt))/1000 ,
        (elapsed_usecs(c2_proft)-elapsed_usecs(c2_virtt))%1000);

      fflush(stdout);
      exit(0);
    }else{/*this is the parent*/
      
      // Start parent on the fibonacci program
      fib=fibonacci(fibarg);
      
      // Wait for children to terminate
      waitpid(0, &status, 0);
      waitpid(0, &status, 0);

      //Read parent itimer values and report them
      getitimer(ITIMER_REAL,&p_realt);
      getitimer(ITIMER_VIRTUAL,&p_virtt);
      getitimer(ITIMER_PROF,&p_proft);

      printf("\n");
      printf("Parent fib = %ld, real time = %ld sec, %ld msec, %ld usec\n",
        fib,
        p_realt_secs,
        elapsed_usecs(p_realt)/1000,
        elapsed_usecs(p_realt)%1000);

      printf("Parent fib = %ld, cpu time = %ld sec, %ld msec, %ld usec\n",
        fib, 
        p_proft_secs,
        elapsed_usecs(p_proft)/1000,
        elapsed_usecs(p_proft)%1000);

      printf("Parent fib = %ld, user time = %ld sec, %ld msec, %ld usec\n",
        fib,
        p_virtt_secs,
        elapsed_usecs(p_virtt)/1000,
        elapsed_usecs(p_virtt)%1000);

      printf("Parent fib = %ld, kernel time = %ld sec, %ld msec, %ld usec\n",
        fib,
        p_proft_secs-p_virtt_secs,
        (elapsed_usecs(p_proft)-elapsed_usecs(p_virtt))/1000,
        (elapsed_usecs(p_proft)-elapsed_usecs(p_virtt))%1000);

      fflush(stdout);
    }
  }
}

long unsigned int fibonacci(unsigned int n){
  if(n==0)
    return 0;
  else if (n==1||n==2)
    return 1;
  else
    return(fibonacci(n-1) + fibonacci(n-2));
} 

//Calculates the total interval time and the remaining time in the timer
//If the interval time is less than the remaining time,
//the function will return 0.
//Otherwise it'll return the difference between the intervaltime and the remaining time.
long elapsed_usecs(struct itimerval timer){
  long timerUsec, remainingUsec;

  timerUsec=(timer.it_interval.tv_sec*1000000) + timer.it_interval.tv_usec;
  remainingUsec=(timer.it_value.tv_sec*1000000) + timer.it_value.tv_usec;
  
  if(timerUsec<remainingUsec){
    return 0;
  }
  return timerUsec-remainingUsec;
}

//signal handlers for each signal
//Increments respective time each time signal is raised.

//Parent signal handler
void p_handler(int signo){
  switch(signo){
  case SIGALRM:
    p_realt_secs++;
    break;
  case SIGVTALRM:
    p_virtt_secs++;
    break;
  case SIGPROF:
    p_proft_secs++;
    break;
  }
}

//Child 1 signal handler
void c1_handler(int signo){
  switch(signo){
  case SIGALRM:
    c1_realt_secs++;
    break;
  case SIGVTALRM:
    c1_virtt_secs++;
    break;
  case SIGPROF:
    c1_proft_secs++;
    break;
  }
}

//Child 2 signal handler
void c2_handler(int signo){
  switch(signo){
  case SIGALRM:
    c2_realt_secs++;
    break;
  case SIGVTALRM:
    c2_virtt_secs++;
    break;
  case SIGPROF:
    c2_proft_secs++;
    break;
  }
}
