#ifndef _RRSched_
#define _RRSched_

#define MAX_CAPACITY 100

struct job{
  int ID;
  int arrivalTime;
  int serviceTime;
};

struct pqueue{
  int size;
  int capacity;
  struct job elements[MAX_CAPACITY];
};

struct pqueue qcreate(void);
void qenqueue(struct job J, struct pqueue Q);
struct job qdequeue(struct pqueue Q);
struct job qcmp(struct job a, struct job b);

#endif
