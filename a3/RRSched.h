#ifndef _RRSched_
#define _RRSched_

struct job{
  int ID;
  int arrivalTime;
  int serviceTime;
};

struct pqueue{
  int size;
  int capacity;
  struct job **elements;
};

struct pqueue *qcreate(int capacity);
void qdelete(struct pqueue *Q);
void qenqueue(struct job *J, struct pqueue *Q);
struct job *qdequeue(struct pqueue *Q);
struct job *qcmp(struct job *a, struct job *b);

#endif
