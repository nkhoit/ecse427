/************************************************************************
 *
 *                ECSE437 - Assignment 1
 *                filename: minishell.c
 *                By: Nguyen Khoi Tran (260350260)
 *
 *
 ***********************************************************************/

/*The shell first finds an executable in the PATH, and then loads and executes it *using execv. Because it uses "." as a separator, it cannot handle file names like *"minshell.h" */

#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include "minishell.h"

char *lookupPath(char **, char **);
int parsCommand(char *, struct command_t*);
int parsePath(char **);
void printPrompt( );
void readCommand(char *);


char *lookupPath(char **argv, char **dir) {
  /*This function searches the directories identified by dir to see if argv[0] (the file name) is there. It allocates a new string, places the full path name in it and then returns the string*/

  char *result;
  char pName[MAX_PATH_LEN];
  int i=0;
  char *directory;

  directory=(char *)malloc(MAX_PATH_LEN+MAX_ARG_LEN+1);
  //Check to see if file name is already an absolute path name
  if(*argv[0]== '/' && access(argv[0],F_OK)) {
   strcpy(directory,"");
   strcpy(directory,argv[0]);
  }

  //Look in PATH directories
  //Use access ( ) to see if the file is in a dir.
  for(i=0; i < MAX_PATHS && dir[i]!=NULL; i++) {
    strcpy(directory,"");
    strcpy(directory,dir[i]);
    strcat(directory,"/");
    strcat(directory,argv[0]);
    if(access(directory,X_OK)==0){
      return directory;
    }
  }
  return NULL;
}

int parseCommand(char *cLine, struct command_t *cmd) {
  int argc;
  char *c1Ptr;
  /*Initialization*/
  c1Ptr = cLine; /*the command line*/
  argc=0;
  cmd->argv[argc] = (char *) malloc(MAX_ARG_LEN);
  /*fill argv[ ]*/
  while((cmd->argv[argc] = (char *)strsep(&c1Ptr, WHITESPACE)) != NULL ) {
    cmd->argv[++argc] = (char *) malloc(MAX_ARG_LEN);
  }

  /*Set the command name and argc*/
  cmd->argc=argc-1;
  cmd->name = (char *) malloc(sizeof(cmd->argv[0]));
  strcpy(cmd->name, cmd->argv[0]);
  return 1;
}

int parsePath(char *dirs[]){
  /* This function reads the PATH variable for this environment, and builds an array, dirs[ ], of the directories in PATH*/

  char *pathEnvVar;
  char *thePath;
  int i=0;

  pathEnvVar = (char *)getenv("PATH");
  thePath = (char *)malloc(strlen(pathEnvVar) + 1);
  strcpy(thePath, pathEnvVar);

  dirs[i]=(char *)malloc(MAX_PATH_LEN);
  while((dirs[i]=(char *)strsep(&thePath,":")) != NULL){
    dirs[++i]=(char *)malloc(MAX_PATH_LEN);
  }

  return 1;
}

void printPrompt( ) {
  printf("%s$ ","nile");
}

void readCommand(char *buffer){
  /*clear buffer then write command*/
  strcpy(buffer,"");
  gets(buffer);
}

int main() {
	/* Shell initialization*/
  char *pathv[MAX_PATHS];
  char commandLine[LINE_LEN];
  struct command_t command;
  pid_t pid;
  int status;
  int escape=1;

	parsePath(pathv);/*Get directory paths from PATH*/

	while(escape){
    printPrompt();

    /*read the command line and parse it*/
    readCommand(&commandLine[0]);

    parseCommand(&commandLine[0], &command);

    /*Get the full pathname for the file*/
    command.name = lookupPath(command.argv, pathv);

    /*Check if command either exits shell,does not exit, or is contained in PATH directory*/
    if(strcmp(command.argv[0],"exit")==0){ //Exits shell
      printf("leaving minishell...\n");
      escape=0; 
    }else if(command.name == NULL){ //Command not found in PATH, return error
      printf("%s: command not found\n",command.argv[0]);
    }else{ //Program found, fork and run process
      /* create child and execute the command*/
      if((pid = fork()) == 0) {
        execvp(command.name, command.argv);
        exit(0);
      }else{
      /*wait for the child to terminate*/
        wait(&status);
      }
    }
  }
  return 1;
}

